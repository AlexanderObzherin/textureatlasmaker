#pragma once

#include <iostream>
#include <unordered_map>
#include <memory>
#include <sstream>
#include <boost/filesystem.hpp>
#include "ImageData.h"

class TextureAtlasMaker
{
public:
	TextureAtlasMaker();
	~TextureAtlasMaker();

	void CreateTextureAtlas( const char* directory );
	
//protected:
	//Load image data grom jpeg file
	std::unique_ptr<ImageData> LoadImageDataFromJpegFile( const char* fileName );
	//Save image data to jpeg file
	void SaveImageToJpegFile( const char* fileName, ImageData& sourceImageData );
	//Load image data grom png file
	std::unique_ptr<ImageData> LoadImageDataFromPngFile( const char* fileName );
	//Save image data to png file
	void SaveImageDataToPngFile( const char* fileName, const ImageData& imageData );

	std::unique_ptr<ImageData> LoadImageDataFromFile( const char* fileName );
	void SaveImageDataToFile( const char* fileName, const ImageData& imageData );

	//Search image files in parameter directory, return vector of filenames with path
	std::vector<std::string> GetImageFilesNames( const char* directory );

	//Helpful method to check directory path
	void CheckDirectory( const char* directory );
	//Method to load images data.
	bool LoadImages( const char* directory );
	//Calculate cell dimensions of one cell as max width (x component) and height (y component) in pixels
	void FindCellDimensions( float& outputWidth, float& outputHeight );
	//Calculate number of rows and columns, trying to aim a square form
	void CalculateGridDimensions( float& outputNumberOfColumns, float& outputNumberOfRows );
	//Draw pixel by pixel image fromImage to toImage at position pos
	void DrawImageOnImageAtPos( float x, float y, ImageData& toImage, ImageData& fromImage );
	//Log informaion to txt file
	void LogMetaData( const std::string& message )
	{
		mMetaDataOutputStream << message << std::endl;
	}

protected:
	//container of images
	std::unordered_map<std::string, ImageData> mImages;

	static const char* const mMetaDataFileName;
	std::ofstream mMetaDataOutputStream;

	static const Color mDefaultPaletteBackgroundColor;
};

