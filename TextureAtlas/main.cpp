#include <iostream>
#include <cstdint>
#include "ImageData.h"
#include "TextureAtlasMaker.h"

int main()
{
	std::cout << "Image Atlas Creator App \n";
	std::cout << "Inpute directory path \n";

	std::string fDirectory;
	std::cin >> fDirectory;

	TextureAtlasMaker mTextureAtlasMaker;
	mTextureAtlasMaker.CreateTextureAtlas( fDirectory.c_str() );
}
