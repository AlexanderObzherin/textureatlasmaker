///For encoding/decoding png files LodePNG library used https://lodev.org/lodepng/

#include "TextureAtlasMaker.h"
#include <cstdint>
#include <string>
#include <unordered_map>
#include <memory>
#include <cmath>
#include <boost/filesystem.hpp>
#include "lodepng.h"

const  char* const TextureAtlasMaker::mMetaDataFileName = "MetaData.txt";
const Color TextureAtlasMaker::mDefaultPaletteBackgroundColor( 0U, 0U, 0U );

TextureAtlasMaker::TextureAtlasMaker()
{
	mMetaDataOutputStream.open( mMetaDataFileName, std::ios_base::app );
}

TextureAtlasMaker::~TextureAtlasMaker()
{

}

void TextureAtlasMaker::CreateTextureAtlas( const char * directory )
{
	if( !LoadImages( directory ) )
	{
		std::cout << "Directory path is incorrect or doesn't contain images. \n";
		return;
	}

	//Find palette specification
	
	//Find dimension of each cell
	float cellWidth;
	float cellHeight;
	FindCellDimensions( cellWidth , cellHeight );

	//Find number of rows and columns
	float numberOfColumns;
	float numberOfRows;
	CalculateGridDimensions( numberOfColumns, numberOfRows );

	//Create palette
	ImageData textureAtlasPalette( static_cast<unsigned int>(cellWidth * numberOfColumns), static_cast<unsigned int>(cellHeight * numberOfRows) );
	textureAtlasPalette.Fill( mDefaultPaletteBackgroundColor );

	auto imageIterator = mImages.begin();

	for( int y = 0; y < numberOfRows; y++ )
	{
		for( int x = 0; x < numberOfColumns; x++ )
		{
			if( imageIterator != mImages.end() )
			{
				DrawImageOnImageAtPos( x * cellWidth, y * cellHeight, textureAtlasPalette, imageIterator->second );

				std::stringstream info;
				info << "Image: " << imageIterator->first << " Position: " << " x: " << x * cellWidth << " y: " << y * cellHeight <<
					" Width: " << imageIterator->second.GetWidth() << " " << " Height: " << imageIterator->second.GetHeight();

				LogMetaData( info.str() );

				imageIterator++;
			}
		}
	}

	std::cout << "Texture atlas created.";

	SaveImageDataToFile( "TextureAtlas.png", textureAtlasPalette );
}

std::unique_ptr<ImageData> TextureAtlasMaker::LoadImageDataFromJpegFile( const char * fileName )
{
	//Implementation mean to use special third-part library like libjpg

	return nullptr;
}

void TextureAtlasMaker::SaveImageToJpegFile( const char * fileName, ImageData & sourceImageData )
{
	//Implementation mean to use special third-part library like libjpg

}

std::unique_ptr<ImageData> TextureAtlasMaker::LoadImageDataFromPngFile( const char * fileName )
{
	//Implementation mean to use special third-part library like libpng
	std::vector<unsigned char> image; //the raw pixels
	unsigned width, height;

	//decode
	unsigned error = lodepng::decode( image, width, height, fileName );

	//if there's an error, display it
	if( error )
	{
		std::cout << "decoder error " << error << ": " << lodepng_error_text( error ) << std::endl;
		return nullptr;
	}

	ImageData imageData( width, height );
   	for( unsigned int y = 0; y < height; y++ )
	{
		for( unsigned int x = 0; x < width*4; x+=4 )
		{
			imageData.SetPixel( x / 4, y, Color( uint8_t(image[width * 4 * y + x]), uint8_t(image[width * 4 * y + x+1]), uint8_t(image[width * 4 * y + x+2]), uint8_t(image[width * 4 * y + x+3]) ));
		}
	}

	//the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
	return std::make_unique<ImageData>(imageData);
}

void TextureAtlasMaker::SaveImageDataToPngFile( const char * fileName, const ImageData & imageData )
{
	std::vector<unsigned char> image;

	///Export image data from ImageData to vector as R, G, B, A... rows from top to bottom.  
	for( unsigned int y = 0; y < imageData.GetHeight(); y++ )
	{
		for( unsigned int x = 0; x < imageData.GetWidth(); x++ )
		{
			image.push_back( static_cast<unsigned char>( imageData.GetPixel( x, y )->r) );
			image.push_back( static_cast<unsigned char>( imageData.GetPixel( x, y )->g ) );
			image.push_back( static_cast<unsigned char>( imageData.GetPixel( x, y )->b ) );
			image.push_back( static_cast<unsigned char>( imageData.GetPixel( x, y )->a ) );
		}
	}

	//Encode the image using 
	unsigned error = lodepng::encode( fileName, image, imageData.GetWidth(), imageData.GetHeight() );

	//if there's an error, display it
	if( error ) std::cout << "encoder error " << error << ": " << lodepng_error_text( error ) << std::endl;
}

std::unique_ptr<ImageData> TextureAtlasMaker::LoadImageDataFromFile( const char * fileName )
{
	return LoadImageDataFromPngFile( fileName );
}

void TextureAtlasMaker::SaveImageDataToFile( const char * fileName, const ImageData & imageData )
{
	SaveImageDataToPngFile( fileName, imageData );
}

std::vector<std::string> TextureAtlasMaker::GetImageFilesNames( const char * directory )
{
	std::vector<std::string> imageFileNames;

	try
	{
		//check if parameter string is directory
		if( boost::filesystem::exists( boost::filesystem::path( directory ) ) && boost::filesystem::is_directory( boost::filesystem::path( directory ) ) )
		{
			boost::filesystem::path pathToDirectory = boost::filesystem::path( directory );

			if( boost::filesystem::is_directory( pathToDirectory ) )
			{
				for( boost::filesystem::directory_entry& x : boost::filesystem::directory_iterator( pathToDirectory ) )
				{
					std::string lookingExtension{ "jpg" };
					size_t dot = x.path().string().find_last_of( "." );

					if( std::string{ "jpg" } == x.path().string().substr( dot + 1 ) || std::string{ "png" } == x.path().string().substr( dot + 1 ) )
					{
						std::cout << "Image file: " << x.path().string() << "\n";
						imageFileNames.push_back( x.path().string() );
					}
				}
			}
		}
	}
	catch( const boost::filesystem::filesystem_error& ex )
	{
		std::cout << ex.what() << '\n';
	}

	return imageFileNames;
}

void TextureAtlasMaker::CheckDirectory( const char * directory )
{
	boost::filesystem::path p = boost::filesystem::path( directory );

	try
	{
		if( boost::filesystem::exists( p ) )
		{
			if( boost::filesystem::is_regular_file( p ) )
			{
				std::cout << p << " size is " << file_size( p ) << '\n';
			}
			else
			{
				if( boost::filesystem::is_directory( p ) )
				{
					std::cout << p << " is a directory containing:\n";

					for( boost::filesystem::directory_entry& x : boost::filesystem::directory_iterator( p ) )
					{
						std::cout << "    " << x.path() << '\n';
					}
				}
				else
				{
					std::cout << p << " exists, but is not a regular file or directory\n";
				}
			}
		}
		else
		{
			std::cout << p << " does not exist\n";
		}

	}
	catch( const boost::filesystem::filesystem_error& ex )
	{
		std::cout << ex.what() << '\n';
	}
}

bool TextureAtlasMaker::LoadImages( const char * directory )
{
	std::vector<std::string> imageFileNames = GetImageFilesNames( directory );

	if( imageFileNames.size() == 0 )
	{
		return false;
	}

	for( auto& it : imageFileNames )
	{
		std::unique_ptr<ImageData> imageData = LoadImageDataFromFile( it.c_str() );
		if( imageData )
		{
			mImages.emplace( it, *imageData );
		}
	}
	return true;
}

void TextureAtlasMaker::FindCellDimensions( float & outputWidth, float & outputHeight )
{
	float height = 0;
	float width = 0;
	for( auto& it : mImages )
	{
		if( it.second.GetHeight() > height )
		{
			height = it.second.GetHeight();
		}
		if( it.second.GetWidth() > width )
		{
			width = it.second.GetWidth();
		}
	}

	outputWidth = width;
	outputHeight = height;
}

void TextureAtlasMaker::CalculateGridDimensions( float & outputNumberOfColumns, float & outputNumberOfRows )
{
	float cellWidth;
	float cellHeight;
	FindCellDimensions( cellWidth, cellHeight );

	int cellsNum = mImages.size();
	int totalSquare = cellWidth * cellHeight * cellsNum;
	float rootSq = sqrtf( totalSquare );

	outputNumberOfColumns = std::ceil( rootSq / cellWidth );
	outputNumberOfRows = std::ceil( (float)(cellsNum) / (float)(outputNumberOfColumns) );
}

void TextureAtlasMaker::DrawImageOnImageAtPos( float x, float y, ImageData & toImage, ImageData & fromImage )
{
	//If fromImage cant be placed entirely, it will be cropped on right and bottom side

	//Source image draw from top left pixel

	//At first check if point pos is inside toImage
	if( !(x >= 0 && y >= 0 && x < toImage.GetWidth() && y < toImage.GetHeight()) )
	{
		return;
	}

	//Crop
	int left;
	if( (toImage.GetWidth() - x) < fromImage.GetWidth() )
	{
		left = toImage.GetWidth() - x;
	}
	else
	{
		left = fromImage.GetWidth();
	}

	int bottom;
	if( (toImage.GetHeight() - y) < fromImage.GetHeight() )
	{
		bottom = toImage.GetHeight() - y;
	}
	else
	{
		bottom = fromImage.GetHeight();
	}

	for( int deltaY = 0; deltaY < bottom; deltaY++ )
	{
		for( int deltaX = 0; deltaX < left; deltaX++ )
		{
			toImage.SetPixel( x + deltaX, y + deltaY, *fromImage.GetPixel( deltaX, deltaY ) );
		}
	}
}
